using System;
using System.IO;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StateSync;
using Newtonsoft.Json;

namespace StateSyncWS
{
    public class Room<StateType, EventType>
    {
        public class Message
        {
            public string method;
            public object payload;
        }

        public class MakeCurrentRequest
        {
            public int last_id;
        }

        private string name;
        private IHistoryPersistor<StateType, EventType> persistor;
        private Timer persistTimer;
        private int hasChanges;
        private History<StateType, EventType> history;
        private ConcurrentDictionary<WebSocket, int?> connectedClients;

        public Room(string name, IHistoryPersistor<StateType, EventType> persistor)
        {
            this.name = name;
            this.persistor = persistor;
            this.history = persistor.Get(name);
            this.connectedClients = new ConcurrentDictionary<WebSocket, int?>();
            this.persistTimer = new Timer(this.PersistThisHistory, null, 5000, 5000);
        }

        private void PersistThisHistory(object _)
        {
            if (Interlocked.Exchange(ref hasChanges, 0) == 1)
            {
                Utils.Retry(() => {
                    persistor.Put(name, history);
                });
            }
        }

        private StateType DeserializeStatePayload(string payload)
        {
            if (typeof(StateType) == typeof(string))
            {
                return (StateType) (object) payload;
            }
            return JsonConvert.DeserializeObject<StateType>(payload);
        }

        private EventType DeserializeEventPayload(string payload)
        {
            if (typeof(EventType) == typeof(string))
            {
                return (EventType) (object) payload;
            }
            return JsonConvert.DeserializeObject<EventType>(payload);
        }

        private async Task<string> WrapReceive(WebSocket webSocket)
        {
            try
            {
                return await webSocket.ReceiveStringAsync();
            }
            catch (Exception)
            {
                connectedClients.TryRemove(webSocket, out _);
                throw;
            }
        }

        private async Task<bool> WrapSend(WebSocket webSocket, string payload)
        {
            try
            {
                await webSocket.SendStringAsync(payload);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                connectedClients.TryRemove(webSocket, out _);
            }
            return false;
        }

        public async Task Join(WebSocket webSocket)
        {
            connectedClients.TryAdd(webSocket, null);

            while (true)
            {
                try
                {
                    string requestString = await WrapReceive(webSocket);
                    if (!string.IsNullOrEmpty(requestString))
                    {
                        await this.ProcessRequest(webSocket, requestString);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    break;
                }
            }

            connectedClients.TryRemove(webSocket, out _);
            await webSocket.CloseAsync(WebSocketCloseStatus.Empty, "", CancellationToken.None);
        }

        private async Task ProcessRequest(WebSocket webSocket, string requestString)
        {
            var request = JsonConvert.DeserializeObject<Message>(requestString);
            switch (request.method)
            {
                case "INITIAL_REQUEST":
                    if (history == null)
                    {
                        await WrapSend(webSocket, JsonConvert.SerializeObject(new Message {
                            method = "NO_STATE"
                        }));
                    }
                    else
                    {
                        var lastState = history.GetLastState();
                        if (await WrapSend(webSocket, JsonConvert.SerializeObject(new Message {
                            method = "INITIAL_PAYLOAD",
                            payload = lastState
                        })))
                        {
                            connectedClients[webSocket] = lastState.Id;
                        }
                    }
                    break;

                case "PUSH_INITIAL_STATE":
                    if (history == null)
                    {
                        history = new History<StateType, EventType>(DeserializeStatePayload(
                            (string) request.payload
                        ));
                        foreach (var client in connectedClients)
                        {
                            if (!client.Value.HasValue)
                            {
                                var lastState = history.GetLastState();
                                if (await WrapSend(client.Key, JsonConvert.SerializeObject(new Message {
                                    method = "INITIAL_PAYLOAD",
                                    payload = lastState
                                })))
                                {
                                    connectedClients[client.Key] = lastState.Id;
                                }
                            }
                        }
                        Interlocked.Exchange(ref hasChanges, 1);
                    }
                    break;

                case "MAKE_CURRENT":
                    if (history != null)
                    {
                        var lastId = JsonConvert.DeserializeObject<MakeCurrentRequest>((string) request.payload).last_id;
                        connectedClients[webSocket] = lastId;
                        var events = history.GetEvents(lastId).ToList();
                        if (events.Any()) {
                            if (await WrapSend(webSocket, JsonConvert.SerializeObject(new Message {
                                method = "EVENT_UPDATE",
                                payload = events
                            })))
                            {
                                connectedClients[webSocket] = events.Last().Id;
                            }
                        }
                    }
                    break;

                case "EVENT_PUSH":
                    if (history != null)
                    {
                        history.AddEvent(DeserializeEventPayload((string) request.payload));
                        foreach (var client in connectedClients)
                        {
                            if (client.Value.HasValue)
                            {
                                var events = history.GetEvents(client.Value.Value).ToList();
                                if (events.Any())
                                {
                                    #pragma warning disable 4014
                                    if (await WrapSend(client.Key, JsonConvert.SerializeObject(new Message {
                                        method = "EVENT_UPDATE",
                                        payload = events
                                    })))
                                    #pragma warning restore 4014
                                    {
                                        connectedClients[client.Key] = events.Last().Id;
                                    }
                                }
                            }
                        }
                        Interlocked.Exchange(ref hasChanges, 1);
                    }
                    break;

                default:
                    break;
            }
        }
    }
}