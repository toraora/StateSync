using System;
using System.Threading;
using System.Threading.Tasks;

namespace StateSyncWS
{
    public class Utils
    {
        public static void Retry(Action fn, int tries = 5)
        {
            while (tries-- > 0)
            {
                try
                {
                    fn();
                    return;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"[{tries} tries left] Caught Exception:\n{e}");
                    Thread.Sleep(25);
                }
            }
        }
    }
}