using System;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace StateSyncWS
{
    public class Lobby<StateType, EventType>
    {
        private ConcurrentDictionary<string, Room<StateType, EventType>> rooms;
        private IHistoryPersistor<StateType, EventType> persistor;
        private ApiModule apiHandler;
        private string StaticBase;

        public Lobby(IConfigurationRoot config, IHistoryPersistor<StateType, EventType> persistor)
        {
            rooms = new ConcurrentDictionary<string, Room<StateType, EventType>>();
            this.persistor = persistor;
            apiHandler = new ApiModule(config);
            StaticBase = config["wwwroot"];
        }

        public Room<StateType, EventType> GetRoom(string name)
        {
            if (rooms.TryGetValue(name, out var room))
                return room;
            return null;
        }

        public void CreateRoom(string name)
        {
            if (rooms.ContainsKey(name))
                throw new ArgumentException($"Room with name {name} already exists");
            rooms[name] = new Room<StateType, EventType>(name, persistor);
        }

        public Room<StateType, EventType> GetOrCreateRoom(string name)
        {
            var room = GetRoom(name);
            if (room == null)
            {
                try
                {
                    CreateRoom(name);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Could not create room:\n{e}");
                }
            }
            room = GetRoom(name);
            return room;
        }

        public async Task Listen(HttpContext context)
        {
            var pathSegments = context.Request.Path.Value.Split('/', StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine($"Got Request at Lobby:Listen, path is: {context.Request.Path.Value}");
            if (pathSegments.Count() > 1 && pathSegments[0] == "ws")
            {
                if (context.WebSockets.IsWebSocketRequest)
                {
                    var webSocket = await context.WebSockets.AcceptWebSocketAsync();
                    var room = GetOrCreateRoom(pathSegments[1]);
                    await room.Join(webSocket);
                }
                else
                {
                    context.Response.StatusCode = 400;
                }
            }
            else if (pathSegments.Count() > 0 && pathSegments[0] == "api")
            {
                await apiHandler.Handle(context);
            }
            else
            {
                var fi = new FileInfo(Path.Combine(this.StaticBase, context.Request.Path.Value.Substring(1)));
                Console.WriteLine($"Static File Request: {fi.FullName}");
                if (fi.Exists)
                {
                    var contents = await File.ReadAllBytesAsync(fi.FullName);
                    var contentLength = contents.Count();

                    context.Response.StatusCode = 200;
                    if (fi.FullName.EndsWith(".html"))
                    {
                        context.Response.ContentType = "text/html";
                        await context.Response.Body.WriteAsync(contents, 0, contentLength);
                    }
                    else if (fi.FullName.EndsWith(".js"))
                    {
                        context.Response.ContentType = "application/javascript";
                        await context.Response.Body.WriteAsync(contents, 0, contentLength);
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                    }
                }
            }
        }
    }
}