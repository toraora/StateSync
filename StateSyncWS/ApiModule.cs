using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;

namespace StateSyncWS
{
    public class ApiModule
    {
        private Dictionary<string, Func<HttpContext, Task>> methods;
        private string _PYTHON_BINARY;
        private string _PARSER_SCRIPT;
        private AuxFiles auxFileManager;

        public ApiModule(IConfigurationRoot config)
        {
            _PYTHON_BINARY = config["Parser.PythonBinary"];
            _PARSER_SCRIPT = config["Parser.ScriptFile"];

            if (_PARSER_SCRIPT.StartsWith("~/"))
            {
                _PARSER_SCRIPT = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                    _PARSER_SCRIPT.Substring(2)
                );
            }

            auxFileManager = new AuxFiles(config);

            methods = new Dictionary<string, Func<HttpContext, Task>>();
            methods["convert_puzzle"] = this.ConvertPuzzle;
        }

        public async Task Handle(HttpContext context)
        {
            context.Response.StatusCode = 400;

            var pathSegments = context.Request.Path.Value.Split('/', StringSplitOptions.RemoveEmptyEntries);
            if (pathSegments.Count() == 1)
            {
                return;
            }

            var apiMethod = pathSegments[1];
            Func<HttpContext, Task> method;
            if (methods.TryGetValue(apiMethod, out method))
            {
                context.Response.StatusCode = 200;
                await method(context);
            }
        }

        internal async Task ConvertPuzzle(HttpContext context)
        {
            using (var postReader = new StreamReader(context.Request.Body)) {
                var puzzleBase64 = await postReader.ReadToEndAsync();
                puzzleBase64 = puzzleBase64.Trim();

                try
                {
                    auxFileManager.PutAux($"puzzle-{DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")}", puzzleBase64);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception trying to save puzzle file:\n{e}");
                }

                var start = new ProcessStartInfo();
                start.FileName = _PYTHON_BINARY;
                start.Arguments = _PARSER_SCRIPT;
                start.UseShellExecute = false;
                start.RedirectStandardInput = true;
                start.RedirectStandardOutput = true;
                start.RedirectStandardError = true;
                using (var process = Process.Start(start))
                {
                    using (var reader = process.StandardOutput)
                    {
                        await process.StandardInput.WriteAsync(puzzleBase64);
                        process.StandardInput.Close();
                        var error = await process.StandardError.ReadToEndAsync();
                        Console.WriteLine(error);
                        var result = await reader.ReadToEndAsync();
                        await context.Response.WriteAsync(result);
                    }
                }
            }
        }
    }
}