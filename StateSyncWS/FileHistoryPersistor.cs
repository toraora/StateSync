using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StateSync;

namespace StateSyncWS
{
    public class FileHistoryPersistor<S, T> : IHistoryPersistor<S, T>
    {
        string BasePath;

        public FileHistoryPersistor(IConfigurationRoot config)
        {
            BasePath = config["FileHistoryPersistor.BasePath"];
            if (BasePath.StartsWith("~/"))
            {
                BasePath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                    BasePath.Substring(2)
                );
            }
            var di = new DirectoryInfo(BasePath);
            if (!di.Exists)
            {
                di.Create();
            }
        }

        public History<S, T> Get(string key)
        {
            var fi = new FileInfo(Path.Combine(this.BasePath, key));
            if (fi.Exists)
            {
                var contents = File.ReadAllText(fi.FullName).Trim();
                if (!string.IsNullOrEmpty(contents))
                {
                    return JsonConvert.DeserializeObject<History<S, T>>(contents);
                }
            }
            return null;
        }

        public void Put(string key, History<S, T> history)
        {
            File.WriteAllText(Path.Combine(this.BasePath, key), JsonConvert.SerializeObject(history));
        }
    }
}