using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StateSync;

namespace StateSyncWS
{
    public class AuxFiles
    {
        string BasePath;

        public AuxFiles(IConfigurationRoot config)
        {
            BasePath = config["AuxFiles.BasePath"];
            if (BasePath.StartsWith("~/"))
            {
                BasePath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                    BasePath.Substring(2)
                );
            }
            var di = new DirectoryInfo(BasePath);
            if (!di.Exists)
            {
                di.Create();
            }
        }

        public void PutAux(string key, string data)
        {
            File.WriteAllText(Path.Combine(this.BasePath, key), data);
        }
    }
}