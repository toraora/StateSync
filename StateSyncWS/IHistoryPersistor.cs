using System;
using StateSync;

namespace StateSyncWS
{
    public interface IHistoryPersistor<S, T>
    {
        History<S, T> Get(string key);

        void Put(string key, History<S, T> history);
    }
}