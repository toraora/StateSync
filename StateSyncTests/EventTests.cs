using System;
using Xunit;

namespace StateSyncTests 
{
    public class EventTests 
    {
        [Fact]
        public void CanConstruct()
        {
            var id = 1;
            var payload = "event";
            var thisEvent = new StateSync.Event<string>(id, payload);
            Assert.Equal(id, thisEvent.Id);
            Assert.Equal(payload, thisEvent.Payload);
        }
    }
}
