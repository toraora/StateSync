using System;
using Xunit;

namespace StateSyncTests 
{
    public class StateTests 
    {
        [Fact]
        public void CanConstruct()
        {
            var id = 1;
            var payload = "state";
            var state = new StateSync.State<string>(id, payload);
            Assert.Equal(id, state.Id);
            Assert.Equal(payload, state.Payload);
        }
    }
}
