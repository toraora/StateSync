using StateSync;
using System;
using Newtonsoft.Json;
using Xunit;

namespace StateSyncTests
{
    public class SerializationTests
    {
        History<string, string> history;

        public SerializationTests()
        {
            var initialState = "initial";
            history = new History<string, string>(initialState);
        }

        [Fact]
        public void DoesSerialize()
        {
            var serialized = JsonConvert.SerializeObject(history);
            Assert.True(serialized.Length > 10);
        }

        [Fact]
        public void DoesDeserialize()
        {
            var serialized = JsonConvert.SerializeObject(history);
            var deserialized = JsonConvert.DeserializeObject<History<string, string>>(serialized);
            Assert.Equal(deserialized.currentId, history.currentId);
            Assert.Equal(deserialized.GetLastState().Payload, history.GetLastState().Payload);
        }
    }
}
