using System;
using System.Linq;
using Xunit;
using StateSync;

namespace StateSyncTests
{
    public class HistoryTests
    {
        History<string, string> history;

        public HistoryTests()
        {
            var initialState = "initial";
            history = new History<string, string>(initialState);
        }

        [Fact]
        public void CheckInitialId()
        {
            Assert.Equal(0, history.GetCurrentId());
        }

        [Fact]
        public void DoesIdIncrement()
        {
            history.AddEvent("event");
            Assert.Equal(1, history.GetCurrentId());
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(2, 1)]
        [InlineData(2, 2)]
        public void DoesReturnCorrectHistory(int numToAdd, int startId)
        {
            for (int i = 0; i < numToAdd; i++)
            {
                history.AddEvent(i.ToString());
            }

            var expectedNumber = numToAdd - startId;
            var eventsSinceStart = history.GetEvents(startId);
            Assert.Equal(eventsSinceStart.Count(), expectedNumber);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(5)]
        public void DoesReturnInitialState(int numToAdd)
        {
            for (int i = 0; i < numToAdd; i++)
            {
                history.AddEvent(i.ToString());
            }

            var initialState = history.GetLastState(numToAdd);
            Assert.Equal("initial", initialState.Payload);
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 1)]
        [InlineData(2, 1, 2)]
        [InlineData(3, 3, 2)]
        public void DoesReturnExpectedCachedState(int numToAdd, int numToSave, int numToQuery)
        {
            for (int i = 0; i < numToAdd; i++)
            {
                history.AddEvent(i.ToString());
            }

            if (numToSave != 0)
            {
                history.AddState(numToSave, numToSave.ToString());
            }

            var expectedState = (numToSave > numToQuery) ? "0" : numToSave.ToString();
            expectedState = expectedState == "0" ? "initial" : expectedState;
            var queriedState = history.GetLastState(numToQuery);
            Assert.Equal(expectedState, queriedState.Payload);
        }
    }
}
