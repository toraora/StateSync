using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;

namespace StateSync
{
    [JsonObject(MemberSerialization.OptIn)]
    public class History<StateType, EventType>
    {
        [JsonProperty]
        public int currentId { get; private set; }
        [JsonProperty]
        public List<Event<EventType>> events { get; private set; }
        [JsonProperty]
        public List<int> cachedStateIds { get; private set; }
        [JsonProperty]
        public Dictionary<int, State<StateType>> cachedStates { get; private set; }

        private object _lock = new object();

        internal History()
        {

        }

        internal History(StateType initialStatePayload)
        {
            this.currentId = 0;
            this.events = new List<Event<EventType>>();
            this.cachedStateIds = new List<int>();
            this.cachedStates = new Dictionary<int, State<StateType>>();

            this.AddState(0, initialStatePayload);
        }

        public void WithLock(Action callback)
        {
            lock (_lock)
            {
                callback();
            }
        }

        public void AddState(int id, StateType statePayload)
        {
            lock (_lock)
            {
                if (this.cachedStates.ContainsKey(id))
                {
                    return;
                }
                this.cachedStates[id] = new State<StateType>(id, statePayload);
                this.cachedStateIds.Add(id);
            }
        }

        public int AddEvent(EventType eventPayload)
        {
            int nextId = -1;
            lock (_lock)
            {
                nextId = this.currentId + 1;
                var theEvent = new Event<EventType>(nextId, eventPayload);
                this.events.Add(theEvent);
                this.currentId = nextId;
            }
            return nextId;
        }

        public IEnumerable<Event<EventType>> GetEvents(int fromId)
        {
            int endId = this.currentId;
            int startId = fromId + 1;
            for (int currentId = startId; currentId <= endId; currentId++)
            {
                lock (_lock)
                {
                    // zero-indexed, but ids are one-indexed
                    yield return this.events[currentId - 1];
                }
            }
        }

        public int GetCurrentId()
        {
            return this.currentId;
        }

        public State<StateType> GetLastState(int fromId = -1)
        {
            if (fromId == -1)
            {
                fromId = GetCurrentId();
            }

            lock (_lock)
            {
                var idx = this.cachedStateIds.BinarySearch(fromId);
                if (idx < 0) // non-exact match
                {
                    // List<T>.BinarySearch returns bitwise complement
                    idx = ~idx - 1;
                }
                return this.cachedStates[this.cachedStateIds[idx]];
            }
        }
    }
}
