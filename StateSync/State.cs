using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading;
using Newtonsoft.Json;

namespace StateSync
{
    [JsonObject(MemberSerialization.OptIn)]
    public class State<T>
    {
        [JsonProperty]
        public int Id { get; private set; }
        [JsonProperty]
        public T Payload { get; private set; }

        [JsonConstructor]
        internal State(int Id, T Payload)
        {
            this.Id = Id;
            this.Payload = Payload;
        }
    }
}
