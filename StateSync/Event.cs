using System;
using Newtonsoft.Json;

namespace StateSync
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Event<T>
    {
        [JsonProperty]
        public int Id { get; private set; }
        [JsonProperty]
        public T Payload { get; private set; }

        [JsonConstructor]
        internal Event(int Id, T Payload)
        {
            this.Id = Id;
            this.Payload = Payload;
        }
    }
}
